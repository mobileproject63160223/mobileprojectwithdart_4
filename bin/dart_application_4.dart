import 'dart:io';

void main() {
  String? checkNum;
  while (checkNum != "exit") {
    var numLottery = stdin.readLineSync()!;
    print(checkWin(numLottery));
    checkNum = numLottery;
  }
  print('ขอบคุณครับ!!!');
}

String checkWin(String w) {
  if (checkLottery1st(w) != 'checkPass') {
    return checkLottery1st(w);
  } else if (checkLottery2nd(w) != 'checkPass') {
    return checkLottery2nd(w);
  } else if (checkLottery3rd(w) != 'checkPass') {
    return checkLottery3rd(w);
  } else if (checkLottery4th(w) != 'checkPass') {
    return checkLottery4th(w);
  } else if (checkLottery5th(w) != 'checkPass') {
    return checkLottery5th(w);
  } else if (checkLotteryPre3num(w) != 'checkPass') {
    return checkLotteryPre3num(w);
  } else if (checkLotteryPost3num(w) != 'checkPass') {
    return checkLotteryPost3num(w);
  } else if (checkLotteryPost2num(w) != 'checkPass') {
    return checkLotteryPost2num(w);
  }
  return 'เสียใจด้วยหมายเลข $w ไม่ได้รับรางวัลใดๆ';
}

List<String> reward1st() {
  var num = ['436594'];
  return num;
}

List<String> reward2nd() {
  var num = ['502412', '285563', '396501', '084971', '049364'];
  return num;
}

List<String> reward3rd() {
  var num = [
    '662884',
    '242496',
    '575619',
    '666926',
    '422058',
    '853019',
    '043691',
    '996939',
    '166270',
    '896753'
  ];
  return num;
}

List<String> reward4th() {
  var num = [
    '996939',
    '043691',
    '422058',
    '853019',
    '662884',
    '166270',
    '666926',
    '896753',
    '242496',
    '575619'
  ];
  return num;
}

List<String> reward5th() {
  var num = [
    '345541',
    '675634',
    '240354',
    '124121',
    '263978',
    '293437',
    '098790',
    '011052',
    '550340',
    '400877'
  ];
  return num;
}

List<String> reward1stNeighbors() {
  var num = ['436593', '436595'];
  return num;
}

List<String> rewardPre3num() {
  var num = ['266', '893'];
  return num;
}

List<String> rewardPost3num() {
  var num = ['282', '447'];
  return num;
}

List<String> rewardPost2num() {
  var num = ['14'];
  return num;
}

String checkLottery1st(String n) {
  if (reward1st().contains(n)) {
    return 'หมายเลข $n ได้รับรางวัลที่1ของสลากกินแบ่งรัฐบาล เป็นจำนวนเงิน 6,000,000 บาท';
  }
  return 'checkPass';
}

String checkLottery2nd(String n) {
  if (reward2nd().contains(n)) {
    return 'หมายเลข $n ได้รับรางวัลที่2ของสลากกินแบ่งรัฐบาล เป็นจำนวนเงิน 200,000 บาท';
  }
  return 'checkPass';
}

String checkLottery3rd(String n) {
  if (reward3rd().contains(n)) {
    return 'หมายเลข $n ได้รับรางวัลที่3ของสลากกินแบ่งรัฐบาล เป็นจำนวนเงิน 80,000 บาท';
  }
  return 'checkPass';
}

String checkLottery4th(String n) {
  if (reward4th().contains(n)) {
    return 'หมายเลข $n ได้รับรางวัลที่4ของสลากกินแบ่งรัฐบาล เป็นจำนวนเงิน 40,000 บาท';
  }
  return 'checkPass';
}

String checkLottery5th(String n) {
  if (reward5th().contains(n)) {
    return 'หมายเลข $n ได้รับรางวัลที่5ของสลากกินแบ่งรัฐบาล เป็นจำนวนเงิน 20,000 บาท';
  }
  return 'checkPass';
}

String checkLottery1stNeighbors(String n) {
  if (reward1stNeighbors().contains(n)) {
    return 'หมายเลข $n ได้รับรางวัลสลากกินแบ่งรัฐบาลที่ใกล้เคียงกับรางวัลที่1มากที่สุด เป็นจำนวนเงิน 100,000 บาท';
  }
  return 'checkPass';
}

String checkLotteryPre3num(String n) {
  String pre = n.substring(0, 3);
  if (rewardPre3num().contains(pre)) {
    return 'หมายเลข $n ได้รับรางวัลสลากกินแบ่งรัฐบาลเลข3ตัวหน้า เป็นจำนวนเงิน 4,000 บาท';
  }
  return 'checkPass';
}

String checkLotteryPost3num(String n) {
  String post = n.substring(3);
  if (rewardPost3num().contains(post)) {
    return 'หมายเลข $n ได้รับรางวัลสลากกินแบ่งรัฐบาลเลข3ตัวท้าย เป็นจำนวนเงิน 4,000 บาท';
  }
  return 'checkPass';
}

String checkLotteryPost2num(String n) {
  String post = n.substring(4);
  if (rewardPost2num().contains(post)) {
    return 'หมายเลข $n ได้รับรางวัลสลากกินแบ่งรัฐบาลเลข2ตัวท้าย เป็นจำนวนเงิน 2,000 บาท';
  }
  return 'checkPass';
}
